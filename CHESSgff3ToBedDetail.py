#!/usr/bin/env python

"Read GFF3 file and write out as BED14 (BED detail) format."

import os,sys,re,optparse
import Mapping.Parser.Bed as Bed
import Mapping.Parser.Gff3 as p

def readGffFile(fname=None):
    models = p.gff2feats(fname)
    models = tweakModels(models)
    return models

def tweakModels(models=None):
    for model in models:
        key_vals=model.getKeyVals()
        if key_vals.has_key('ID'):
            del(key_vals['ID']) # redundant with displayid
        if key_vals.has_key("geneID"):
            val = key_vals["geneID"]
            del(key_vals["geneID"])
            key_vals["field13"]=val
        if key_vals.has_key("producer"):
            val = key_vals["producer"]
            del(key_vals["producer"])
            key_vals["origin"]=val
        field14 = ""
        for key in key_vals.keys():
            if key == 'field13':
                continue
            if key == 'field14':
                continue
            val = key_vals[key]
            if len(field14)>0:
                field14 = field14 + ";"
            field14 = field14 + key+"="+val
        if len(field14) > 0:
            key_vals["field14"]=field14
    return models

def writeBedFile(fname=None,feats=None):
    Bed.feats2bed(feats,fname=fname,bed_format='BED14')

def convert(bed_file=None,
            gff_file=None):
    feats = readGffFile(fname=gff_file)
    writeBedFile(fname=bed_file,feats=feats)

def main(bed_file=None,gff_file=None):
    convert(bed_file=bed_file,
            gff_file=gff_file)

if __name__ == '__main__':
    usage = "%prog [options]"
    parser = optparse.OptionParser(usage)
    parser.add_option("-g","--gff_file",help="GFF3 file to convert. Can be compressed. [required]",dest="gff_file"),
    parser.add_option("-b","--bed_file",help="BED14 (bed detail) format file to write [required]",
                      dest="bed_file",default=None)
    (options,args)=parser.parse_args()
    if not options.bed_file or not options.gff_file:
        parser.error("Bed or GFF files not specified.")
    main(gff_file=options.gff_file,
         bed_file=options.bed_file)
