"""Functions for reading alternative splicing files."""

import Mapping.FeatureModel as feature
import Utils.General as utils
import sys
import warnings

def getFeats(fname=None,fh=None):
    return altsplice2feats(fname=fname,fh=fh)

def altsplice2feats(fname=None,fh=None):
    """
    Function: read features from an alternative splicing format file
    Returns : list of DNASeqFeature objects representing alternatively spliced
              regions
    Args    : fname - name of file to read
    """
    if not fh and fname:
        fh = utils.readfile(fname)
    else:
        raise ValueError("AltSpliceCounts requires name of file or file stream.")
    header = fh.readline()
    lineNumber = 1
    feats = []
    while 1:
        line = fh.readline()
        lineNumber = lineNumber + 1
        if not line:
            fh.close()
            break
        toks = line.rstrip().split('\t')
        key_vals = {}
        gene_id = toks[0]
        key_vals['gene_id']=gene_id
        display_id=toks[1]
        key_vals['S']=toks[2]
        key_vals['L']=toks[3]
        seqname=toks[4]
        start=int(toks[5])
        end=int(toks[6])
        length=end-start
        strand=int(toks[8])
        key_vals['altsplice_type']=toks[9]
        feat_type = "alternatively spliced region"
        feat = feature.DNASeqFeature(seqname=seqname,
                                     display_id=display_id,
                                    start=start,
                                    length=length,
                                    strand=strand,
                                    key_vals=key_vals,
                                    feat_type=feat_type)
        feats.append(feat)
    return feats

            
        
