#!/usr/bin/env python

import unittest
import os,sys

import Mapping.FeatureModel as M
import Mapping.Parser.PhytozomeGff as p
import Mapping.Parser.Bed as b

class Bed2Feats(unittest.TestCase):

   test_bed = 'data/PhytozomeGFF/AT1G07350.2.bed'
   test_gff = 'data/PhytozomeGFF/AT1G07350.2_gene_exons.gff3'
   bedFeatsDict = {}
   bedFeats = None
   gffFeats = None
   
   def setUp(self):
      self.bedFeats = b.bed2feats(self.test_bed)
      for feat in self.bedFeats:
         self.bedFeatsDict[feat.getDisplayId()]=feat
      self.gffFeats = p.gff2feats(fname=self.test_gff)
         
   def testExonNumber(self):
      "Exon numbers should match."
      for feat in self.gffFeats:
         my_exons = feat.getSortedFeats(feat_type='exon')
         bedfeat = self.bedFeatsDict[feat.getDisplayId()]
         other_exons = bedfeat.getSortedFeats(feat_type='exon')
         self.assertEquals(len(my_exons),len(other_exons))

   def testExonCoords(self):
      "Exons coordinates should match."
      for feat in self.gffFeats:
         my_exons = feat.getSortedFeats(feat_type='exon')
         bedfeat = self.bedFeatsDict[feat.getDisplayId()]
         other_exons = bedfeat.getSortedFeats(feat_type='exon')
         i = 0
         for my_exon in my_exons:
            other_exon = other_exons[i]
            self.assertEquals(my_exon.getStart(),other_exon.getStart())
            self.assertEquals(my_exon.getLength(),other_exon.getLength())
            self.assertEquals(my_exon.getStrand(),other_exon.getStrand())
            i = i + 1
                          
if __name__ == "__main__":
    unittest.main()
    
