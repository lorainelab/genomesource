#!/usr/bin/env python

"""
Convert BED to GTF.  
"""

import fileinput, sys, optparse
import Mapping.Parser.Bed as b

# for command line use
def main(args=None, include_gene=None,producer=None):
    feats=getFeats(args)
    writeFeats(feats, include_gene,producer)

# for command line use
def getFeats(fname=None):
    feats=[]
    for line in fileinput.input(fname):
        feat=b.bedline2feat(line)
        feats.append(feat)
    return feats

# for interactive use
def getFeatsFromFile(fname=None):
    feats=[]
    fh=open(fname)
    while 1:
        line = fh.readline()
        if not line:
            break
        else:
            feat=b.bedline2feat(line.rstrip())
            feats.append(feat)
    return feats

# By default (include_gene=None), a place-holder option is passed backe to writeFeats and to getExtraFeat
def getGeneId(feat, include_gene, display_id):
    gene_id=None
    if include_gene==None:
        gene_id='not a gene id'
    elif include_gene==13:
        gene_id=feat.getVal('symbol')
    elif include_gene==4:
        toks = display_id.split('.')
        transcript_num = toks[-1]
        gene_id = '.'.join(toks[0:-1])
    else:
        raise ValueError("Only values of 4 and 13 are accepted.")
    if not isinstance(gene_id, str):
        raise ValueError("Could not retrieve gene_id.")
    return gene_id


def getExtraFeat(display_id, gene_id, include_gene):
    extra_feat=None
    if include_gene==None:
        extra_feat='transcript_id "%s";'%display_id
    else: #change this to something better
        extra_feat='transcript_id "%s"; gene_id "%s";'%(display_id,gene_id)
    return extra_feat


def writeFeats(feats, include_gene, producer):
    for feat in feats:
        exons=feat.getFeats('exon')
        display_id=feat.getDisplayId()
        gene_id=getGeneId(feat, include_gene, display_id)
        extra_feat=getExtraFeat(display_id, gene_id, include_gene)
        for exon in exons:
            seqname=exon.getSeqname() #does not need to be in this for loop
            start=exon.getStart()+1
            end=exon.getEnd()
            strand=exon.getStrand() #does not need to be in this for loop
            if strand==1:
                strand='+'
            else:
                strand='-'
            vals=[seqname,producer,'exon',str(start),str(end),'.',strand,'.',extra_feat]
            sys.stdout.write('\t'.join(vals)+'\n')


if __name__ == '__main__':
    usage = '%prog [options] [FILE...]'
    parser=optparse.OptionParser(usage)
    parser.add_option('-g','--gene_id_field',
                      help="include gene id in extra feature field using this field from the bed file (4 or 13)",
                      dest='include_gene',default=None, type='int', action="store")
    parser.add_option('-p','--producer',
                      help="name of producer, for field 2 [optional]",
                      dest="producer",default="NA",type="string")
    (options,args)=parser.parse_args()
    main(args=args,include_gene=options.include_gene,
         producer=options.producer)
