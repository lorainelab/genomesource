#!/usr/bin/env python

ex=\
"""
Merge gene model description and gene models into bed detail. See UCSC Genome Bioinformatics Web site for more info.

Bed detail has 14 fields. First 12 are the same as ordinary BED used for gene models. Next two fields are:

  Field 13 - Gene symbol (can be locus id)
  Field 14 - Human-friendly descriptive text about the gene

When loaded into IGB, the description field is searchable and is displayed in the Selection Info table. BED detail format is described on the UCSC Web site.

"""

import Utils.General as utils
import os,sys,optparse

def readDetails(fn=None,
                header=False,
                id_field=0,
                sym_field=None,
                descr_field=1,
                sep='\t'):
    """
    Function: read a gene info file and return the info
    Returns : dictionary, keys are ?? values description
    Args    : fn - string, name of the file with gene details
              id_field - int, the column with gene model id (should match whatever's in 
                         column 3 (zero-based) of the BED file [default is 0]
              sym_field - int, column with human-friendly gene symbol (usually
                          assigned by geneticists studying the gene) [optional]
              descr_field - int, column with human-friendly description
                            [default is 1]
              sep - string, field separator [default is tab]
    """ 
    d = {} # make a new dictionary
    fh = utils.readfile(fn)
    txt=fh.read()
    lines=txt.splitlines() # handles \r and \n
    n = 0 # line number 
    if header:
        lines=lines[1:]
        n=n+1
    for line in lines:
        n = n + 1
        if line.startswith('#'):
            continue
        toks = line.split(sep)
        # Note: sometimes fields are empty
        key=toks[id_field]
        symbol=None
        if sym_field and toks[sym_field]!='':
            symbol=toks[sym_field]
        else:
            # try to guess the gene symbol from
            # gene model id (key)
            symbol=guessLocus(key)
        descr=toks[descr_field]
        if descr=='':
            descr='NA'
        if d.has_key(key):
            if not(d[key][1]==descr and d[key][0]==symbol):
                raise ValueError("line %i says %s has different values %s and %s"%
                                 (n,key,symbol,descr))
        d[key]=[symbol,descr]
    return d

def newBed(fn=None,newfn=None,d=None,force=False):
    """
    Function: Create a new bed file that contains two new fields,
              gene symbol (column 13) and gene description 
              (column 14)
    Returns : 
    Args    : fn - string, name of the bed file to read
              newfn - string, name of new file to write
              d - dictionary, output from readDetails 
              force - clobber current field 13 and 14 if present [default is False, don't clobber]

    Note that the file with gene information parsed by readDetails might not contain
    information for every model in the bed file. When that happens, we'll use "Not Available"
    in the descrition fields and a gene id parsed from the name in the gene name field. If
    we can't parse the gene id from the name field, then we'll just use the name. 
    """
    b = utils.readfile(fn)
    if not newfn:
        n = sys.stdout
    else:
        n = open(newfn,'w')
    while 1:
        line = b.readline()
        if not line:
            b.close()
            break
        toks = line.rstrip().split('\t')
        # this che
        if not force and len(toks)>=14:
            raise ValueError("File %s already has 14 or more fields."%fn)
        name = toks[3]
        descr = None
        symbol = None
        if d.has_key(name):
            symbol=d[name][0]
            descr=d[name][1]
        else:
            symbol=guessLocus(name)
            if d.has_key(symbol):
                descr=d[symbol][1]
            else:
                descr='Not Available'
        if len(toks) == 14:
            if force:
                toks[12]=symbol
                toks[13]=descr
        else:
            toks.append(symbol)
            toks.append(descr)
        newline = '\t'.join(toks)+'\n'
        n.write(newline)
    if newfn:
        n.close()

# Try to guess the locus id for field 13 of the new BED file by
# parsing the locus id. Many groups use LOCUS.N as transcript ids,
# for example Pp1s1_59V6.1 is gene model 1 from gene Pp1s1_59V6
# in Physcomitrella patens. So if we can't find the approved gene
# symbol in our input data, try to guess it. If we can't guess it,
# then return name.
def guessLocus(name):
    name_toks=name.split('.')
    if len(name_toks)>1 and isInt(name_toks[-1]):
        # using .var naming
        symbol='.'.join(name_toks[0:-1])
    else:
        symbol = name 
    return symbol

# find out if s is an int or not
def isInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

def main(bed_file=None,gene_info_file=None,out_file=None,
         descr_field=None,id_field=None,sym_field=None,force=None,
         header=None):
    d = readDetails(fn=gene_info_file,id_field=id_field,
                    descr_field=descr_field,sym_field=sym_field,
                    header=header)
    newBed(fn=bed_file,
           d=d,
           newfn=out_file,
           force=force)

if __name__ == '__main__':
    usage = "%prog [options]\n"+ex
    parser = optparse.OptionParser(usage)
    parser.add_option("-g","--gene_info_file",help="Tab or comma delimited file with descriptive information for gene models.",dest="gene_info_file"),
    parser.add_option("-i","--id_field",help="Index of id field, zero-base, of the annotation file [default is 0]",
                     type="int",dest="id_field",default=0)
    parser.add_option("-d","--descr_field",help="Index of description field, zero-based [default is 1]",
                      type="int",dest="descr_field",default=1)
    parser.add_option("-s","--sym_field",default=None,type=int,
                      help="Index of symbol field, zero-based [optional]")
    parser.add_option("-b","--bed_file",help="BED file to read",dest="bed_file")
    parser.add_option("-o","--out_file",help="BED14 file to write [optional, default is print to stdout]",
                      dest="out_file",default=None),
    parser.add_option("-H","--Header",default=False,dest="header",
                      help="Gene info file has a header [default is no header]",
                      action="store_true")
    parser.add_option("-f","--force",help="Clobber current field 13 and 14 if present [default is don't clobber].",
                      action="store_true",default=False,dest="force")
    (options,args)=parser.parse_args()
    if not options.bed_file or not options.gene_info_file:
        parser.error("Bed or gene info files not specified.")
    main(gene_info_file=options.gene_info_file,
         bed_file=options.bed_file,
         out_file=options.out_file,
         id_field=options.id_field,
         sym_field=options.sym_field,
         descr_field=options.descr_field,
         header=options.header,
         force=options.force)

"""
ex)

bedToBedDetail.py -b P_patens_Jan_2008.bed.gz -d 4 -i 0 -f -H -g cosmoss.genonaut.description.txt.gz -o P_patens_Jan_2008.bed 
sort -k1,1 -k2,2n P_patens_Jan_2008.bed | bgzip > P_patens_Jan_2008.bed.gz 
tabix -s 1 -b 2 -e 3 P_patens_Jan_2008.bed.gz
"""
