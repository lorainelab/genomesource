#!/usr/bin/env python

"""Convert IGBQuickLoad.org URLs to relative paths."""

import sys,os,re

reg=re.compile(r'http://(www.)?igbquickload.org/quickload/',re.I)

def convert():
    dirs=os.listdir('.')
    for d in dirs:
        if d == '.svn':
            continue
        try:
            os.chdir(d)
            sys.stderr.write("processing %s\n"%d)
        except OSError:
            continue
        fh = open("annots.xml")
        lines = fh.read()
        fh.close()
        newlines = reg.sub('',lines)
        fh=open('annots.xml','w')
        fh.write(newlines)
        os.chdir('..')

# run this in the top level of a QL directory
if __name__ == '__main__':
    convert()


