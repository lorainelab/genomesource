#!/usr/bin/env python

"""Not tested. Just a draft."""

"""
This script reads in two fasta files, compares them, displays if the files contain the same sequence or not. 
"""

import sys,os,optparse
from Bio import SeqIO

def readSeq(file=None):
    handle = open(file,'r')
    record_dict=SeqIO.to_dict(SeqIO.parse(handle,'fasta'))
    return record_dict

def compSeqs(a=None,b=None):
    "Compare fasta files a and b. If they're different, return False. Otherwise, return True"
    a_dict = readSeq(file=a)
    b_dict = readSeq(file=b)
    comp = {}
    for name in a_dict.keys():
        if not b_dict.has_key(name):
            return False
        a_record_seq = a_dict[name].seq
        b_record_seq = b_dict[name].seq
        if not a_record_seq.tostring() == b_record_seq.tostring():
            return False
        else:
            comp[name]=True # remember we did the comparison
    for name in b_dict.keys():
        if comp.has_key(name):
            continue # we already checked it
        else:
            if not a_dict.has_key(name):
                return False
            a_record_seq = a_dict[name].seq
            b_record_seq = b_dict[name].seq
            if not a_record_seq.tostring() == b_record_seq.tostring():
                return False
    return True

if __name__=='__main__':
    usage = '%prog file1 file2\n\nPrint whether two fasta files contain the same sequences.'
    parser=optparse.OptionParser(usage)
    (options,args)=parser.parse_args()
    if not len(args)==2:
        parser.print_help()
        exit(0)
    a = args[0]
    b = args[1]
    if compSeqs(a=a,b=b):
        print "%s and %s have the same sequence."%(a,b)
    else:
        print "%s and %s don't have the same sequence."%(a,b)
