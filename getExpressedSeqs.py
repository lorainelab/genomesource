#!/usr/bin/env python

"""Retrieve the EST and mRNA sequences for sequenced plant genomes from Plant GDB"""

import sys,urllib,os

the_latest = 187
# see also ftp://ftp.plantgdb.org/download/FASTA_187/EST/
# mappings between genome version directories and PlantGDB naming schemes for files
gversion2est = """A_lyrata_Apr_2011,Arabidopsis_lyrata
A_thaliana_Jun_2009,Arabidopsis_thaliana
B_distachyon_Aug_2010,Brachypodium_distachyon
C_reinhardtii_Nov_2010,Chlamydomonas_reinhardtii,Chlamydomonas_sp__CCMP681,Chlamydomonas_sp__HS_5,Chlamydomonas_sp__W80
O_sativa_japonica_Jun_2009,Oryza_sativa_Japonica_Group,Oryza_sativa_Indica_Group,Oryza_sativa"""

# for info on these files, see:
# ftp://ftp.plantgdb.org/README.txt

# use this to download plant species EST and mRNA sequences from PlantGDB
FTP = "ftp://ftp.plantgdb.org/download/FASTA_"

def getESTs(genbank_version=the_latest,
            gversion2est=gversion2est,
            quickload='.'):
    """
    Function: Download from EST sequences from PlantGDB
    Args    : genbank_version - an integer corresponding to a PlantGDB (GenBank) version number
              gversion2est - string representing genome versions and EST files
              quickload - string, root of quickload directory
    Returns :

    Invoke this in root of QuickLoad directory.
    This method expects sequence files to reside in
    ftp://ftp.plantgdb.org/download/.
    """
    tokss = map(lambda x:x.split(','),gversion2est.split('\n'))
    for toks in tokss:
        gversion = toks[0]
        for prefix in toks[1:]:
            fn = prefix+'.mRNA.EST.fasta'
            qfn = quickload+os.sep+gversion+os.sep+fn
            try:
                open(qfn)
            except IOError:
                # not yet downloaded
                ffn = FTP+str(genbank_version)+'/EST/'+fn
                cmd = 'wget %s %s'%(ffn,qfn)
                sys.stderr.write("starting: " + cmd + "\n")
                os.system(cmd)
                sys.stderr.write("done: " + cmd + "\n")

    
def doit(genbank_version=the_latest,genus=None,species=None):
    """
    Function: Download from PlantGDB ftp site the mRNA and EST sequences
              for the given species.
              If already downloaded, do nothing.
    Args    : genbank_version - an integer corresponding to a PlantGDB (GenBank) version number
              genus - string, taxonomic name of genus for the sequenced genome
              species - string, taxonomic name of the species for sequenced genome
    Returns :

    This method expects sequence files to reside in ftp://ftp.plantgdb.org/download/.
    """
    getEst(genbank_version=genbank_version,genus=genus,species=species)
    getMrna(genbank_version=genbank_version,genus=genus,species=species)

def setupDirs(genbank_version=None):
    """
    Function: Set up the directories into which sequences will be downloaded.
              If already set up, do nothing.
    Args    : genbank_veresion - an integer corresponding to a PlantGDB (GenBank) version number
    Returns :

    Note that the sequences will be downloaded into a directory named for the
    genbank_version. For example, if genbank_version is 180, then the sequences
    will appear in a directory called FASTA_172 with subdirectories EST and mRNA.PLN,
    following the naming scheme of PlantGDB.
    """
    fdir = "FASTA_"+str(genbank_version)
    try:
        os.listdir(fdir)
    except OSError:
        os.mkdir(fdir)
    est_dir = fdir + os.sep + 'EST'
    try:
        os.listdir(est_dir)
    except OSError:
        os.mkdir(est_dir)
    mrna_dir = fdir + os.sep + 'mRNA.PLN'
    try:
        os.listdir(mrna_dir)
    except OSError:
        os.mkdir(mrna_dir)   

def getEst(genbank_version=None,genus=None,species=None):
    """
    Function: Download from PlantGDB ftp site the EST sequences
              for the plant identified by the given genus and species
              If the file is already downloaded, do nothing.
    Args    : genbank_version - an integer corresponding to a PlantGDB (GenBank) version number
              genus - string, name of genus
              species - string, name of species
    Returns :

    This method expects remote sequence files to reside in ftp://ftp.plantgdb.org/download/.
    """
    setupDirs(genbank_version=genbank_version)
    d = 'FASTA_'+str(genbank_version)+os.sep+'EST'
    curdir = os.getcwd()
    os.chdir(d)
    f = genus+'_'+species+'.mRNA.EST.fasta'
    try:
        open(f)
    except IOError:
        url = FTP+str(genbank_version)+'/EST/'+f
        cmd = 'curl -O '+url+' '+f
        sys.stderr.write("starting: " + cmd + "\n")
        os.system(cmd)
        sys.stderr.write("done: " + cmd + "\n")
    os.chdir(curdir)
    sys.stderr.write("Done ESTs for: "  + genus + ' ' + species + '\n')
                    
def getMrna(genbank_version=None,genus=None,species=None):
    """
    Function: Download from PlantGDB ftp site the mRNA sequences
              for the plant identified by the given genus and species
              If the file is already downloaded, do nothing.
    Args    : genbank_version - an integer corresponding to a PlantGDB (GenBank) version number
              genus - string, name of genus
              species - string, name of species
    Returns :

    This method expects remote sequence files to reside in ftp://ftp.plantgdb.org/download/.
    """
    setupDirs(genbank_version=genbank_version)
    d = 'FASTA_'+str(genbank_version)+os.sep+'mRNA.PLN'
    curdir = os.getcwd()
    os.chdir(d)
    f = genus+'_'+species + '.mRNA.PLN.fasta'
    try:
        open(f)
    except IOError:
        url = FTP+str(genbank_version)+'/mRNA.PLN/'+f
        cmd = 'curl -O '+url+' '+f
        sys.stderr.write("starting: " + cmd + "\n")
        os.system(cmd)
        sys.stderr.write("done: " + cmd + "\n")
    os.chdir(curdir)
    sys.stderr.write("Done mRNAs for: " + genus + ' ' + species + "\n")
        
def usage():
    """
    Function: Print a usage message for this script when it is run from
              command line as a stand-alone program
    Args    : 
    Returns :
    """
    sys.stderr.write("USAGE: get_seqs.py genbank_version\n\n")
    sys.stderr.write("see: ftp://ftp.plantgdb.org/download/FASTA_* for possible version values.\n")
    
    
if __name__ == "__main__":
    try:
        genbank_version = sys.argv[1]
    except IndexError:
        usage()
        exit(1)
    doitAll(genbank_version)
