"A script that builds gene models from maizesequence.org GFF"

"""Info about the file:

$ gunzip -c ZmB73_5b_FGS.gff.gz | cut -f 1 | sort | uniq
1
10
2
3
4
5
6
7
8
9
Mt
Pt
UNKNOWN
$ gunzip -c ZmB73_5b_FGS.gff.gz | cut -f 2 | sort | uniq
ensembl
$ gunzip -c ZmB73_5b_FGS.gff.gz | cut -f 3 | sort | uniq
CDS
chromosome
exon
gene
intron
mRNA

$ gunzip -c ZmB73_5b_FGS.gff.gz | cut -f 6 | sort | uniq
.
$ gunzip -c ZmB73_5b_FGS.gff.gz | cut -f 7 | sort | uniq
+
-
.
$ gunzip -c ZmB73_5b_FGS.gff.gz | cut -f 8 | sort | uniq
.
0
1
2

"""
import Utils.General as g
import Mapping.FeatureFactory as factory
import Mapping.Parser.Bed as Bed
import sys

def getBiotypes(feats):
    "d is output from makeModels"
    bd = {}
    for feat in feats:
        biotype = feat.getVal('biotype')
        if biotype:
            if not bd.has_key(biotype):
                bd[biotype]=[feat]
            else:
                bd[biotype].append(feat)
    return bd

def makeModels(fn="ZmB73_5b_FGS.gff.gz"):
    "Read the file and make models"
    fh = g.readfile(fn)
    d = {}
    biotypes = {}
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        toks = line.rstrip().split('\t')
        feat_type = toks[2]
        if feat_type == 'chromosome' or feat_type=='gene':
            continue
        chrom = toks[0]
        start = toks[3]
        end = toks[4]
        extra_feat = toks[-1]
        strand = toks[6]
        vals = extra_feat.split(';')
        # get attributes from extra feature field of GFF
        valss = map(lambda x:x.split('='),vals)
        # print valss
        subd = {}
        # just checking
        for vals in valss:
            key = vals[0]
            val = vals[1]
            if not subd.has_key(key):
                subd[key]=val
            else:
                raise ValueError("Extra feat has too many values for: %s from line %s"%(key,line))
        name = subd['Name']
        parent_name = subd['Parent']
        try:
            biotype = subd['biotype']
            if biotypes.has_key(biotype):
                biotypes[biotype]=biotypes[biotype]+1
            else:
                biotypes[biotype]=1
        except KeyError:
            pass
        if feat_type == 'mRNA':
            # do something
            if d.has_key(name):
                raise ValueError("Error: saw mRNA with this name before: %s"%name)
            else:
                feat = factory.makeFeatFromGffFields([chrom,'MaizeGenome.org',feat_type,
                                                      start,end,0,strand],cfeat=True)
            feat.setDisplayId(name)
            d[name]=feat
        else:
            feat = factory.makeFeatFromGffFields([chrom,'MaizeGenome.org',feat_type,
                                                 start,end,0,strand],cfeat=False)
            # depends on features appearing in the file in a certain way
            d[parent_name].addFeat(feat)
        feat.setKeyVals(subd)
    return d

def writeBedFile(feats=None,fn=None):
    if not fn:
        fh = sys.stdout
    else:
        fh = open(fn,'w')
    for feat in feats.values():
        fh.write(Bed.feat2bed(feat))
    if fn:
        fh.close()
